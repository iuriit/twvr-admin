import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import loginPageStyle from "assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx";

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // register form
      registerUsername: "",
      registerUsernameState: "",
      registerPassword: "",
      registerPasswordState: "",
      registerConfirmPassword: "",
      registerConfirmPasswordState: "",
    };
    this.registerClick = this.registerClick.bind(this);
  }
  componentDidMount() {
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  // function that verifies if a string has a given length or not
  verifyLength(value, length) {
    if (value.length >= length) {
      return true;
    }
    return false;
  }
  // function that verifies if two strings are equal
  compare(string1, string2) {
    if (string1 === string2) {
      return true;
    }
    return false;
  }  
  change(event, stateName, type, stateNameEqualTo) {
    this.setState({[stateName] : event.target.value});
    switch (type) {
      case "verifyLength":
        if (this.verifyLength(event.target.value, 1)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      case "equalTo":
        if (this.compare(event.target.value, this.state[stateNameEqualTo])) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      default:
        break;
    }
  }
  registerClick() {
    if (this.state.registerUsernameState === "") {
      this.setState({ registerUsernameState: "error" });
    }
    if (this.state.registerPasswordState === "") {
      this.setState({ registerPasswordState: "error" });
    }
    if (this.state.registerConfirmPasswordState === "") {
      this.setState({ registerConfirmPasswordState: "error" });
    }

    if(this.state.registerUsernameState === "success" 
      && this.state.registerPasswordState === "success"
      && this.state.registerConfirmPasswordState === "success") {
      
    }
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <GridContainer style={{position: 'absolute', left: 0, top: 0, backgroundColor: 'rgb(0, 188, 212)', width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
          <GridItem xs={12} sm={6} md={4}>
            <form>
              <Card login>
                <CardHeader
                  className={`${classes.cardHeader} ${classes.textCenter}`}
                  color="primary"
                >
                  <h4 className={classes.cardTitle}>Register</h4>
                </CardHeader>
                <CardBody>
                  <CustomInput
                    success={this.state.registerUsername === "success"}
                    error={this.state.registerUsernameState === "error"}
                    labelText="Username"
                    id="username"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Face className={classes.inputAdornmentIcon} />
                        </InputAdornment>
                      ),
                      onChange: event => this.change(event, "registerUsername", "verifyLength"),
                      type: "text",
                    }}
                  />
                  <CustomInput
                    success={this.state.registerPassword === "success"}
                    error={this.state.registerPasswordState === "error"}
                    labelText="Password"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon className={classes.inputAdornmentIcon}>
                            lock_outline
                          </Icon>
                        </InputAdornment>
                      ),
                      onChange: event => this.change(event, "registerPassword", "verifyLength"),
                      type: "password",
                    }}
                  />
                  <CustomInput
                    success={this.state.registerConfirmPassword === "success"}
                    error={this.state.registerConfirmPasswordState === "error"}
                    labelText="Confirm Password"
                    id="confirm_password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <Icon className={classes.inputAdornmentIcon}>
                            lock_outline
                          </Icon>
                        </InputAdornment>
                      ),
                      onChange: event => this.change(event, "registerConfirmPassword", "equalTo", "registerPassword"),
                      type: "password",
                    }}
                  />
                </CardBody>
                <CardFooter className={classes.justifyContentCenter}>
                  <Button color="primary" size='lg' round onClick={this.registerClick}>
                    Register
                  </Button>
                </CardFooter>
                <CardFooter className={classes.justifyContentCenter}>
                  <Button color="rose" simple size='lg' block>
                    Login
                  </Button>
                </CardFooter>
              </Card>
            </form>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

RegisterPage.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(loginPageStyle)(RegisterPage);
