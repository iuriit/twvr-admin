import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'admin-on-rest';
import { Globals } from '../../Globals';

export default (type, params) => {
    // called when the user attempts to log in
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        // alert(username + "," + password);
        const request = new Request(Globals.url_login, {
            method: 'POST',
            body: JSON.stringify({ user:{
                username: username,
                password: password,
            }}),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })
        // alert(Globals.url_login);
        return fetch(request)
            .then(response => {
                if(response.status !== 200 && response.status !== 401) {
                    throw new Error(response.errors);
                }
                return response.json();
            })
            .then(data => {
                if(data.errors !== undefined) {
                    var errKey = Object.keys(data.errors)[0];
                    var errValue = Object.values(data.errors)[0];
                    throw new Error(errKey + " " + errValue);
                } else if(data.user !== undefined) {
                    for (let key in data.user) {
                        localStorage.setItem(key, data.user[key]);
                    }
                    document.location.reload();
                    // return Promise.resolve();
                } else {
                    throw new Error('Something went wrong. Please try again.');
                }
            });
    }
    // called when the user clicks on the logout button
    if (type === AUTH_LOGOUT) {
        localStorage.clear();
        return Promise.resolve();
    }
    // called when the API returns an error
    if (type === AUTH_ERROR) {
        const { status } = params;
        if (status === 401 || status === 403) {
            localStorage.clear();
            return Promise.reject();
        }
        return Promise.resolve();
    }
    // called when the user navigates to a new location
    if (type === AUTH_CHECK) {
        return localStorage.getItem('token') ? Promise.resolve() : Promise.reject();
    }
    return Promise.reject('Unknown method');
};