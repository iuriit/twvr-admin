import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userLogin } from 'admin-on-rest';
import { MuiThemeProvider } from '@material-ui/core/styles';

// @material-ui/core components
// import InputAdornment from "@material-ui/core/InputAdornment";
// import Icon from "@material-ui/core/Icon";

// @material-ui/icons
// import Face from "@material-ui/icons/Face";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import { Globals } from '../../Globals';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoginPage: true,
      // login form
      loginUsername: "",
      loginUsernameState: "",
      loginPassword: "",
      loginPasswordState: "",      
      // register form
      registerUsername: "",
      registerUsernameState: "",
      registerPassword: "",
      registerPasswordState: "",
      registerConfirmPassword: "",
      registerConfirmPasswordState: "",
    };
    this.loginClick = this.loginClick.bind(this);
    this.registerClick = this.registerClick.bind(this);
  }

  // function that verifies if a string has a given length or not
  verifyLength(value, length) {
    if (value.length >= length) {
      return true;
    }
    return false;
  }

  // function that verifies if two strings are equal
  compare(string1, string2) {
    if (string1 === string2) {
      return true;
    }
    return false;
  }  

  change(value, stateName, type, stateNameEqualTo) {
    this.setState({[stateName] : value});
    switch (type) {
      case "verifyLength":
        if (this.verifyLength(value, 1)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      case "equalTo":
        if (this.compare(value, this.state[stateNameEqualTo])) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      default:
        break;
    }
  }

  loginClick() {
    var loginUsername = document.getElementById("login_username").value;
    var loginPassword = document.getElementById("login_password").value;
    var success = true;

    if(!this.verifyLength(loginUsername, 1)) {
      success = false;
      this.setState({ loginUsernameState: "error" });
    }
    if(!this.verifyLength(loginPassword, 1)) {
      success = false;
      this.setState({ loginPasswordState: "error" });
    }

    if(success) {
      this.setState({
        loginUsernameState: "success",
        loginPasswordState: "success",
      });
      
      const credentials = {
        username: loginUsername,
        password: loginPassword,
      };
      this.props.userLogin(credentials);
    }
  }

  registerClick() {
    var registerUsername = document.getElementById("register_username").value;
    var registerPassword = document.getElementById("register_password").value;
    var registerConfirmPassword = document.getElementById("register_confirm_password").value;
    var success = true;
  
    if(!this.verifyLength(registerUsername, 1)) {
      success = false;
      this.setState({ registerUsernameState: "error" });
    }
    if(!this.verifyLength(registerPassword, 1)) {
      success = false;
      this.setState({ registerPasswordState: "error" });
    }
    if(!this.compare(registerPassword, registerConfirmPassword)) {
      success = false;
      this.setState({ registerConfirmPasswordState: "error" });
    }

    if(success)
    {
      this.setState({
        registerUsernameState: "success",
        registerPasswordState: "success",
        registerConfirmPasswordState: "success",
      });
      this.registerUser(registerUsername, registerPassword);
    }
  }

  registerUser(username, password) {
    const request = new Request(Globals.url_register, {
      method: 'POST',
      body: JSON.stringify({ user:{
          username: username,
          password: password,
          role: "user",
      }}),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    })
    return fetch(request)
      .then(response => {
        if(response.status !== 200 && response.status !== 401) {
          this.setState({ registerUsernameState: "error" });
        } else {
          this.setState({ isLoginPage: true });
        }
      })
  }

  render() {
    if (this.state.isLoginPage) {
      return (
        <MuiThemeProvider theme={this.props.theme}>
          <form>
            <GridContainer style={{position: 'absolute', left: 0, top: 0, backgroundColor: 'rgb(0, 188, 212)', width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <GridItem xs={12} sm={6} md={4}>
                <Card login>
                  <CardHeader
                    color="rose"
                    style={{textAlign: 'center'}}
                  >
                    <h4 >Log in</h4>
                  </CardHeader>
                  <CardBody>
                    <CustomInput
                      success={this.state.loginUsernameState === "success"}
                      error={this.state.loginUsernameState === "error"}
                      labelText="Username"
                      id="login_username"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: event => this.change(event.target.value, "loginUsername", "verifyLength"),
                        type: "text",
                      }}
                    />
                    <CustomInput
                      success={this.state.loginPasswordState === "success"}
                      error={this.state.loginPasswordState === "error"}
                      labelText="Password"
                      id="login_password"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: event => this.change(event.target.value, "loginPassword", "verifyLength"),
                        type: "password",
                      }}
                    />
                  </CardBody>
                  <CardFooter style={{justifyContent: 'center'}}>
                    <Button color="rose" size='lg' round onClick={this.loginClick}>
                      Log in
                    </Button>
                  </CardFooter>
                  {/* <CardFooter>
                    <Button color="primary" simple size='lg' block onClick={() => this.setState({isLoginPage: false})}>
                      Register
                    </Button>
                  </CardFooter> */}
                </Card>
              </GridItem>
            </GridContainer>
          </form>
        </MuiThemeProvider>
      );
    } else {
      return (
        <MuiThemeProvider theme={this.props.theme}>
          <form>
            <GridContainer style={{position: 'absolute', left: 0, top: 0, backgroundColor: 'rgb(0, 188, 212)', width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <GridItem xs={12} sm={6} md={4}>
                <Card login>
                  <CardHeader
                    color="primary"
                    style={{textAlign: 'center'}}
                  >
                    <h4 >Register</h4>
                  </CardHeader>
                  <CardBody>
                    <CustomInput
                      success={this.state.registerUsernameState === "success"}
                      error={this.state.registerUsernameState === "error"}
                      labelText="Username"
                      id="register_username"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: event => this.change(event.target.value, "registerUsername", "verifyLength"),
                        type: "text",
                      }}
                    />
                    <CustomInput
                      success={this.state.registerPasswordState === "success"}
                      error={this.state.registerPasswordState === "error"}
                      labelText="Password"
                      id="register_password"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: event => this.change(event.target.value, "registerPassword", "verifyLength"),
                        type: "password",
                      }}
                    />
                    <CustomInput
                      success={this.state.registerConfirmPasswordState === "success"}
                      error={this.state.registerConfirmPasswordState === "error"}
                      labelText="Confirm Password"
                      id="register_confirm_password"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: event => this.change(event.target.value, "registerConfirmPassword", "equalTo", "registerPassword"),
                        type: "password",
                      }}
                    />
                  </CardBody>
                  <CardFooter style={{justifyContent: 'center'}}>
                    <Button color="primary" size='lg' round onClick={this.registerClick}>
                      Register
                    </Button>
                  </CardFooter>
                  <CardFooter>
                    <Button color="rose" simple size='lg' block onClick={() => this.setState({isLoginPage: true})}>
                      Log in
                    </Button>
                  </CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
          </form>
        </MuiThemeProvider>
      );
    }
  }
};

export default connect(undefined, { userLogin })(Login);