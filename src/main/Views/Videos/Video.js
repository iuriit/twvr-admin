import React from 'react';
import { List, Edit, Create, Datagrid, TextField, FileInput, FileField, ImageInput, ImageField, EditButton, DisabledInput, LongTextInput, ReferenceInput, required, SelectInput, SimpleForm, TextInput } from 'admin-on-rest';

export const VideoList = (props) => (
    <List {...props} title="Videos" >
        <Datagrid>
            <TextField source="id" />
            <ImageField source="thumbnail" />
            <TextField source="title" />
            <TextField source="description" />
            <TextField source="category" />
            <TextField source="tag" />
            <TextField source="owner" />
            <EditButton />
        </Datagrid>
    </List>
);

export const VideoCreate = (props) => (
    <Create {...props}>
        <SimpleForm submitOnEnter={false}>
            <ReferenceInput label="Category" source="category" reference="category" validate={required} allowEmpty>
                <SelectInput optionText="name" />
            </ReferenceInput>
            <ImageInput source="thumbnail" label="Thumbnail" accept="video/*">
                <ImageField source="src" title="thumbnail" />
            </ImageInput>
            <FileInput source="video" label="Video" accept="video/*">
                <FileField source="src" title="title" />
            </FileInput>
            <TextInput source="title" />
            <LongTextInput source="description" />
            <TextInput source="tag" />
        </SimpleForm>
    </Create>
);

const VideoTitle = ({ record }) => {
    return <span>Video {record ? `"${record.title}"` : ''}</span>;
};

export const VideoEdit = (props) => (
    <Edit title={<VideoTitle />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            {/* <ReferenceInput label="Category" source="category" reference="category" validate={required} allowEmpty>
                <SelectInput optionText="name" />
            </ReferenceInput> */}
            <TextInput source="category" />
            <TextInput source="title" />
            <LongTextInput source="description" />
            <TextInput source="tag" />
        </SimpleForm>
    </Edit>
);