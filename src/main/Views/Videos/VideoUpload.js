import React from 'react';
import { post } from 'axios';
import { Globals } from '../../Globals';
var Spinner = require('react-spinkit')

export default class VideoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      category: null,
      title: null,
      description: null,
      tag: null,
      thumbnail: null,
      video: null,
      uploading: false,
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.videoUpload = this.videoUpload.bind(this)
    this.onCategoryChange = this.onCategoryChange.bind(this)
    this.onTitleChange = this.onTitleChange.bind(this)
    this.onDescriptionChange = this.onDescriptionChange.bind(this)
    this.onTagChange = this.onTagChange.bind(this)
    this.onThumbnailChange = this.onThumbnailChange.bind(this)
    this.onVideoChange = this.onVideoChange.bind(this)
  }
  componentDidMount() {
  }
  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    this.setState({uploading: true});
    this.videoUpload().then((response)=>{
      this.setState({uploading: false});
      if(response.status === 200) {
        this.props.history.push('/video');
      }
      else {
        alert(response.data.errors);
      }
    })
  }
  videoUpload(){
    const url = Globals.url_base + "/video";
    const formData = new FormData();
    formData.append('category', this.state.category);
    formData.append('title', this.state.title);
    formData.append('description', this.state.description);
    formData.append('tag', this.state.tag);
    formData.append('thumbnail', this.state.thumbnail);
    formData.append('video', this.state.video);

    const token = localStorage.getItem('token');
    const config = {
        headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + token
        }
    }

    return post(url, formData, config);
  }
  onCategoryChange(e) {
    this.setState({category:e.target.value})
  }
  onTitleChange(e) {
    this.setState({title:e.target.value})
  }
  onDescriptionChange(e) {
    this.setState({description:e.target.value})
  }
  onTagChange(e) {
    this.setState({tag:e.target.value})
  }
  onThumbnailChange(e) {
    this.setState({thumbnail:e.target.files[0]})
  }
  onVideoChange(e) {
    this.setState({video:e.target.files[0]})
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          <h1>File Upload</h1>
          <div>
            <h3>Category</h3>
            <input name="category" type="text" onChange={this.onCategoryChange}/>
          </div>

          <div>
            <h3>Title</h3>
            <input name="title" type="text" onChange={this.onTitleChange}/>
          </div>
  
          <div>
            <h3>Description</h3>
            <input name="description" type="text" onChange={this.onDescriptionChange}/>
          </div>
  
          <div>
            <h3>Tag</h3>
            <input name="tag" type="text" onChange={this.onTagChange}/>
          </div>
  
          <div>
            <h3>Thumbnail</h3>
            <input name="thumbnail" type="file" accept="image/*" onChange={this.onThumbnailChange}/>
          </div>
  
          <div>
            <h3>Video</h3>
            <input name="video" type="file" accept="video/*" onChange={this.onVideoChange}/>
          </div>
  
          <div>
            <h3>Upload</h3>
            <div>
              <button type="submit">Upload</button>
              {
                this.state.uploading ? <Spinner name='circle'/> : null
              }
            </div>
          </div>
        </form>
      </div>
   )
  }
}
