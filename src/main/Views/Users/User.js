import React from 'react';
import { List, Edit, Create, Datagrid, TextField, EditButton, DisabledInput, SimpleForm, TextInput } from 'admin-on-rest';

export const UserList = (props) => (
  <List {...props} title="Users" >
    <Datagrid>
      {/* <TextField source="id" /> */}
      <TextField source="username" />
      <TextField source="role" />
      <EditButton />
    </Datagrid>
  </List>
);

export const UserCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="username" />
      <TextInput source="password" type="password" />
    </SimpleForm>
  </Create>
);

const UserName = ({ record }) => {
  return <span>User {record ? `"${record.username}"` : ''}</span>;
};

export const UserEdit = (props) => (
  <Edit title={<UserName />} {...props}>
    <SimpleForm>
      <DisabledInput source="username" />
      <TextInput source="password" type="password" />
    </SimpleForm>
  </Edit>
);