import React from 'react';
import { List, Edit, Create, Datagrid, TextField, EditButton, DisabledInput, SimpleForm, TextInput } from 'admin-on-rest';

export const CategoryList = (props) => (
    <List {...props} title="Categories" >
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="owner" />
            <EditButton />
        </Datagrid>
    </List>
);

export const CategoryCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            {/* <ReferenceInput label="User" source="userId" reference="users" validate={required} allowEmpty>
                <SelectInput optionText="name" />
            </ReferenceInput> */}
            <TextInput source="name" />
        </SimpleForm>
    </Create>
);

const CategoryName = ({ record }) => {
    return <span>Kategori {record ? `"${record.name}"` : ''}</span>;
};

export const CategoryEdit = (props) => (
    <Edit title={<CategoryName />} {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            {/* <ReferenceInput label="User" source="userId" reference="users" validate={required}>
                <SelectInput optionText="name" />
            </ReferenceInput> */}
            <TextInput source="name" />
            {/* <LongTextInput source="body" /> */}
        </SimpleForm>
    </Edit>
);