const url_base = 'http://18.130.247.68:3000/api';
// const url_base = 'http://localhost:3000/api';

export const Globals = {
    url_base: url_base,
    url_login: url_base + '/login',
    url_register: url_base + '/register',
};