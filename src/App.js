import React from 'react';
import { jsonServerRestClient, fetchUtils, Admin, Resource } from 'admin-on-rest';
import addUploadCapabilities from './main/AddUploadCapabilities';

import authClient from './main/Views/Auth/AuthClient';
import Login from './main/Views/Auth/Login';
import { UserList, UserCreate, UserEdit } from './main/Views/Users/User';
import { VideoList, VideoEdit } from './main/Views/Videos/Video';
import { CategoryList, CategoryCreate, CategoryEdit } from './main/Views/Categories/Category';
import { Delete } from 'admin-on-rest';
import { Globals } from './main/Globals';

import VideoUpload from './main/Views/Videos/VideoUpload';

const httpClient = (url, options = {}) => {
  if (!options.headers) {
      options.headers = new Headers({ 
        'Accept': 'application/json',
        // 'Content-Type': 'multipart/form-data' 
      });
  }
  const token = localStorage.getItem('token');
  options.headers.set('Authorization', `Bearer ${token}`);
  return fetchUtils.fetchJson(url, options);
}

const restClient = jsonServerRestClient(Globals.url_base, httpClient);
const uploadCapableClient = addUploadCapabilities(restClient);

const App = () => (
  <Admin title="TWVR Admin" loginPage={Login} authClient={authClient} restClient={uploadCapableClient}>
    <Resource name='video' list={VideoList} create={VideoUpload} edit={VideoEdit} remove={Delete} />
    <Resource name='category' list={CategoryList} create={CategoryCreate} edit={CategoryEdit} remove={Delete} />
    {
      (localStorage.getItem('role') !== undefined && localStorage.getItem('role') === 'admin') ? 
        <Resource name='user' list={UserList} create={UserCreate} edit={UserEdit} remove={Delete} />
        : null
    }
  </Admin>
);

export default App;