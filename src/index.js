import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// import LoginPage from './main/Views/Auth/LoginPage.js';
// import RegisterPage from './main/Views/Auth/RegisterPage.js';

import "assets/scss/material-dashboard-pro-react.css?v=1.4.0";

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
